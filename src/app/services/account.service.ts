import { Injectable } from "@angular/core";
import { Area } from "../models/area.model";
import { Building } from "../models/building.model";
import { Channel } from "../models/channel.model";
import { Floor } from "../models/floor.model";
import { DataServices } from "./data.service";
import { NaviServices } from "./navi.service";

@Injectable()
export class AccountService {
    buildings: Building[] = [];
    navi = new NaviServices();
    valueType :string = 'avg'; //indicates the metric to be used to calculate all data in the system: avg, sum, min, max
    sensorType: string = '1'; //indicates the sensor used on the system: 0: sensr, 1: utilizr, 2:
    fromDate: Date;
    tillDate: Date;
    dataTables:any[] = [];
    breadcrumbs:any[] = [];

    constructor(private api: DataServices){
        this.fromDate = this.api.getDateFrom();
        this.tillDate = this.api.getDateTill();
    }

    getUnitFromSensorType(sensorType: string){
        let unit:string = '';
        switch(sensorType){
            case '0':
                unit = 'KwH';
                break;
            case '1':
                unit = 'Occupancy percentage';
                break;            
        }
        return unit;
    }

    getTitleFromSensorType(sensorType: string){
        let title:string = '';
        switch(sensorType){
            case '0':
                title = 'Energy generated (' + this.getUnitFromSensorType(sensorType) + ')';
                break;
            case '1':
                title = this.getUnitFromSensorType(sensorType) + ' %';
                break;            
        }
        return title;
    }    

    setBuildings(buildings:Building[]){
        this.buildings = buildings;
    }

    getBuildings(){
        return this.buildings;
    }

    saveInSessionStorage(){
        sessionStorage.setItem('dataStructure', JSON.stringify(this.buildings));
        this.navi.saveNavigationData();
        sessionStorage.setItem('valueType', this.valueType);
        sessionStorage.setItem('sensorType', this.sensorType);
        sessionStorage.setItem('fromDate', this.fromDate.toString());
        sessionStorage.setItem('tillDate', this.tillDate.toString());
    }

    loadFromSessionStorage(){
        let valueType = sessionStorage.getItem('value');
        if(valueType != null) {
            this.valueType = valueType;
        }
        let sensorType = sessionStorage.getItem('sensorType');
        if(sensorType != null) {
            this.sensorType = sensorType;
        }
        let fromDate = sessionStorage.getItem('fromDate');
        if(fromDate != null) {
            this.fromDate = new Date(fromDate);
        }        
        let tillDate = sessionStorage.getItem('tillDate');
        if(tillDate != null) {
            this.tillDate = new Date(tillDate);
        }
        if(this.fromDate.getDate() == this.tillDate.getDate()){
            this.fromDate.setDate(this.fromDate.getDate()-1);
        }
        let jsonObject = sessionStorage.getItem("dataStructure");
        this.buildings = [];
        if(jsonObject!=null) {
            let jsonData = [];
            jsonData = JSON.parse(jsonObject); 
            for(let i=0; i<jsonData.length; i++){
                let building = new Building(jsonData[i]);          
                this.buildings.push(building);
            }
        }
        this.navi.loadNavigationData();
    }
}