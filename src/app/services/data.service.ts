import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
//import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { environment as env } from 'src/environments/environment';
import { calendarSearch } from '../models/calendarSearch.model';
import { SensorType } from '../models/measurements/sensorType.model';
import { MeasurementType } from '../models/measurements/measurementType.model';
import { ObjectType } from '../models/measurements/objectType.model';
import { Measurement } from '../models/measurements/measurement';
import { HeaderTemplateDirective } from '@progress/kendo-angular-grid';

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json", "Authorization": "c31z" })
  };

@Injectable()
export class DataServices{
    public consoleTag: string = "API";

    private datepipe: DatePipe = new DatePipe('en-US');
    private baseUrl:string;
    private tenantId:string;
    private fromUrl:string;
    private finalUrl:string;
    private profileJson:string;
    private timeRange:number;
    private refreshTime:number;
    private dateFrom:Date;
    private dateTill:Date;
    private headers: HttpHeaders

    constructor(private httpClient: HttpClient){
        this.baseUrl = env.enappgyData.serverUrl;
        this.tenantId = env.enappgyData.tenantId;
        this.timeRange = 1; //in days
        this.refreshTime = 3600;
        this.fromUrl = '';
        this.dateFrom = new Date();
        this.dateFrom.setDate(this.dateFrom.getDate()-1);
        this.dateTill = new Date();        
        this.finalUrl = '';
        this.profileJson='';
        this.headers = new HttpHeaders();
    }

    setDateFrom(date: Date){
        this.dateFrom = date;
    }

    getDateFrom(){
        return this.dateFrom;
    }

    setDateTill(date: Date){
        this.dateTill = date;
    }

    getDateTill(){
        return this.dateTill;
    }

    setTenantId(tenantId:string){
        this.tenantId = tenantId;
    }

    getTenantId(){
        return this.tenantId;
    }

    setTimeRange(timeRange:number){
        this.timeRange = timeRange;
    }

    getTimeRange(){
        return this.timeRange;
    }

    getFinalUrl(){
        return this.finalUrl;
    }

    dateToString(myDate:Date){
        let date = this.datepipe.transform(myDate, 'dd-MMM-YYYY');
        return date;
    }

    getFromUrl(){
        this.fromUrl = "from=" + this.dateFrom + "&till=" + this.dateTill;
        console.log(this.consoleTag, this.finalUrl);
        return this.fromUrl;
    }

    getDataBucket (objectId:string, sensorType:string){
        this.finalUrl = this.baseUrl + '/Data/'  + this.tenantId + '/' + objectId + '/buckets?'
            + "from=" + this.dateFrom.toISOString() + "&till=" + this.dateTill.toISOString() + '&sensorType=' + sensorType;// + '&bucketSizeInSeconds=' + this.refreshTime;
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(encodeURI(this.finalUrl));
    }

    getUtilization (objectId:string, sensorType:string, searchValues: calendarSearch){
        this.finalUrl = this.baseUrl + '/Data/'  + this.tenantId + '/' + objectId + '/utilization?' +
            "dateStart=" + this.datepipe.transform(searchValues.minDate, 'yyyy-MM-dd')  + 
            "&dateEnd=" + this.datepipe.transform(searchValues.maxDate, 'yyyy-MM-dd') + 
            '&timeStart=' + searchValues.timeInterval[0] + '&timeEnd=' + searchValues.timeInterval[1] + 
            '&weekDays=' + searchValues.weekDays + '&sensorType=' + sensorType;
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(encodeURI(this.finalUrl));
    }

    getBuilding(buildingId:string){
        //this.finalUrl =  env.enappgyData.metadataUrl + '/' + this.tenantId + '/Buildings/' + buildingId;
        //this.finalUrl =  env.enappgyData.metadataUrl + '/'  + 'Buildings/' + this.tenantId + '/' + buildingId;
        this.finalUrl = env.enappgyData.metadataUrl + '/' + 'Buildings' + '/' + buildingId;
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(this.finalUrl);        
    }

    getBuildings(){
        //this.finalUrl = env.enappgyData.metadataUrl + '/' + this.tenantId + '/Buildings/';
        this.finalUrl = env.enappgyData.metadataUrl + '/Buildings/GetByTenantId/' + this.tenantId; //+ '/GetAll' ; //esto no deberia estar aqui
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(this.finalUrl);
    }

    getBuildingFullStructure(measurement: Measurement){
        this.finalUrl = env.enappgyData.dataUrl + '/Data/' + this.tenantId + "/" + measurement.ObjectId + "/MeasurementsByBuildingWithFloorsAreas?" +
        "dateStart=" + this.datepipe.transform(measurement.CalendarSearch.minDate, 'yyyy-MM-dd')  + 
        "&dateEnd=" + this.datepipe.transform(measurement.CalendarSearch.maxDate, 'yyyy-MM-dd') + 
        '&timeStart=' + measurement.CalendarSearch.timeInterval[0] + '&timeEnd=' + measurement.CalendarSearch.timeInterval[1] + 
        '&weekDays=' + measurement.CalendarSearch.weekDays + '&sensorType=' + measurement.SensorType + '&measurementType=' + measurement.MeasurementType + '&objectType=' + measurement.ObjectType;
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(this.finalUrl);       
    }

    getFloors(objectId:string){
        //this.finalUrl = env.enappgyData.metadataUrl + '/' + 'Buildings/'  + this.tenantId + '/' + objectId + '/floors';
        this.finalUrl = env.enappgyData.metadataUrl + '/Floors/' + 'GetByBuildingIdWithAreas/'  + objectId;        
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(this.finalUrl);
    }

    getAreas(objectId:string){ // TODO: FIX endpoint url. add tenant  // + this.tenantId
        this.finalUrl = env.enappgyData.metadataUrl + '/Floors/' + objectId + '/areas';
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(this.finalUrl);
    }

    getSensorsInArea(objectId:string){// TODO: add tenant to route '/' + this.tenantId +
        this.finalUrl = env.enappgyData.metadataUrl + '/Areas/' + objectId;
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(this.finalUrl);
    }

    getSensorsInFloor(objectId:string){// TODO: Add tenant + this.tenantId +
        this.finalUrl = env.enappgyData.metadataUrl + '/Floors/' + objectId + '/sensors';
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(this.finalUrl);
    }

    async getMeasurement_Legacy (objectId:string, sensorType: SensorType, searchValues: calendarSearch, measurementType: MeasurementType, objectType: ObjectType ){
        this.finalUrl = this.baseUrl + '/Data/'  + this.tenantId + '/' + objectId + '/measurements?' +
            "dateStart=" + this.datepipe.transform(searchValues.minDate, 'yyyy-MM-dd')  + 
            "&dateEnd=" + this.datepipe.transform(searchValues.maxDate, 'yyyy-MM-dd') + 
            '&timeStart=' + searchValues.timeInterval[0] + '&timeEnd=' + searchValues.timeInterval[1] + 
            '&weekDays=' + searchValues.weekDays + '&sensorType=' + sensorType + '&measurementType=' + measurementType + '&objectType=' + objectType;
        console.log(this.consoleTag, "legacy");
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(encodeURI(this.finalUrl), { headers: this.headers });
    }
    
    getMeasurement (measurement: Measurement){        
        this.headers.set("keepAlive","false");
        this.finalUrl = this.baseUrl + '/Data/'  + this.tenantId + '/' + measurement.ObjectId + '/measurements?' +
            "dateStart=" + this.datepipe.transform(measurement.CalendarSearch.minDate, 'yyyy-MM-dd')  + 
            "&dateEnd=" + this.datepipe.transform(measurement.CalendarSearch.maxDate, 'yyyy-MM-dd') + 
            '&timeStart=' + measurement.CalendarSearch.timeInterval[0] + '&timeEnd=' + measurement.CalendarSearch.timeInterval[1] + 
            '&weekDays=' + measurement.CalendarSearch.weekDays + '&sensorType=' + measurement.SensorType + '&measurementType=' + measurement.MeasurementType + '&objectType=' + measurement.ObjectType;       
         console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(encodeURI(this.finalUrl));
    }

    getMeasurementArray (measurement: Measurement){  
        this.headers.set("keepAlive","false");
        //this.finalUrl = this.baseUrl + '/Data/'  + this.tenantId + '/' + measurement.ObjectId + '/measurementsbyday?' +
        this.finalUrl = this.baseUrl + '/Data/'  + this.tenantId + '/' + measurement.ObjectId + '/measurementsbyPeriod?' +
            "dateStart=" + this.datepipe.transform(measurement.CalendarSearch.minDate, 'yyyy-MM-dd')  + 
            "&dateEnd=" + this.datepipe.transform(measurement.CalendarSearch.maxDate, 'yyyy-MM-dd') + 
            '&timeStart=' + measurement.CalendarSearch.timeInterval[0] + '&timeEnd=' + measurement.CalendarSearch.timeInterval[1] + 
            '&weekDays=' + measurement.CalendarSearch.weekDays + '&sensorType=' + measurement.SensorType + '&measurementType=' + measurement.MeasurementType + '&objectType=' + measurement.ObjectType +
            '&bucketTimeWindow=' + measurement.BucketTimeWindow;
        console.log(this.consoleTag, this.finalUrl);
        return this.httpClient.get(encodeURI(this.finalUrl));
    }
}