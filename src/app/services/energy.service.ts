import { Injectable } from "@angular/core";
import { Floor } from "../models/floor.model";
import { Measurement } from "../models/measurements/measurement";
import { calendarSearch } from '../models/calendarSearch.model';
import { SensorType } from "../models/measurements/sensorType.model";
import { MeasurementType } from "../models/measurements/measurementType.model";
import { ObjectType } from "../models/measurements/objectType.model";
import { Energy } from "../models/energy.model";
import { DataServices } from "./data.service";
import { DatePipe } from "@angular/common";
import { Subject } from "rxjs/internal/Subject";
import { Observable } from "rxjs/internal/Observable";
import moment from "moment";

@Injectable({
  providedIn: 'root'

})
export class EnergyService {
  energy: Energy = new Energy();
  energyPrevious!: Energy;
  private datepipe: DatePipe = new DatePipe('en-EU');
  
  private energyCurrent$: Subject<Energy>;
  getEnergyCurrentYear$(): Observable<Energy> {
    return this.energyCurrent$.asObservable();
  };
  private energyPrevious$: Subject<Energy>;
  getEnergyPreviousYear$(): Observable<Energy> {
    return this.energyPrevious$.asObservable();
  };
  private energySavings$: Subject<Energy>;
  getEnergySavings$(): Observable<Energy> {
    return this.energySavings$.asObservable();
  };
 
  constructor(private api: DataServices) { 
    this.energySavings$ = new Subject<Energy>();
    this.energyPrevious$ = new Subject<Energy>();
    this.energyCurrent$ = new Subject<Energy>();
  }

  async getBuildingEnergySavings(Floor: Floor[], Search: calendarSearch) {
    let minDate = Search.minDate.toDateString();
    let maxDate = Search.maxDate.toDateString();
    /*esta funcion de getEnergy hace iteraciones de medicion por piso lo que la hace asincrona
    por lo tanto necesitamos un observable no una promesa ya que se ira actualizando el monto
    con cada llamada deberemos actualizar una variable interna (propiedad)
    */
    /*let currentYear = await this.getEnergy(Floor, Search, minDate, maxDate, enums.calculationType.currentYear);
    this.energyCurrent$.next(currentYear);
    let prevYear = await this.getEnergy(Floor, Search, minDate, maxDate, enums.calculationType.previousYear);
    this.energyPrevious$.next(prevYear);*/

  }
  

  async getEnergy(Floor: Floor[], s: calendarSearch, startDate: string, endDate: string): Promise<Energy> {
  
    let en = new Energy();
    let dateRange = await this.getDatesBetween(startDate, endDate);
     Floor.forEach(async (f: Floor) => {
      await dateRange.forEach(async (dt: Date) => {
        let measurement: Measurement = new Measurement();
        measurement.CalendarSearch = s;
        measurement.ObjectId = f.id;
        measurement.SensorType = SensorType.Energy;
        measurement.MeasurementType = MeasurementType.Sum;
        measurement.ObjectType = ObjectType.Area;
        await this.getMeasurement(measurement, dt);
        //aqui debemos sumar a la var interna (propiedad) para que el observable notifique updates
      });
    });
    let time = (parseInt(s.timeInterval[1])  - parseInt(s.timeInterval[0])) * dateRange.length;
    en.total = (this.energy.total * time ) / 1000;
    return en;
  }
  
  async getPreviousYearSearch(search: calendarSearch): Promise<calendarSearch>{
    let prevSearch = new calendarSearch();
    prevSearch.timeInterval = search.timeInterval;
    prevSearch.weekDays = search.weekDays;
    prevSearch.minDate = search.minDate;
    prevSearch.maxDate = search.maxDate;

    let cminDate = prevSearch.minDate;
    let cYear = prevSearch.minDate.getFullYear();
    let newYear = cYear - 1
    cminDate.setFullYear(newYear);
    let cmaxDate = prevSearch.maxDate;
    cmaxDate.setFullYear(newYear);
    prevSearch.minDate = cminDate;
    prevSearch.maxDate = cmaxDate;
    return prevSearch;
  }
  
  //async con Promise<Energy> originalmente
  calculateEnergySavings(current: Energy, previous: Energy) : Energy {
    let eSavings = new Energy();

    let tmpEnergyPrev : number = 0;
    let tmpEnergyCurrent : number = 0;

    if(current !== undefined && previous !== undefined) {
      current.timeLine.forEach(cEnergy => {
        let prevEnergy = previous.timeLine.find(x=> x.date = cEnergy.date);
        if(prevEnergy !== undefined){
          console.log("energyService", cEnergy.value + " - " + prevEnergy.value);
          //como los datos vienen vacios meteemos en codigo duro para testear
          //hay que revisar como se calcula estas lineas no las comprendo bien..que debemos graficar
          //OJO ABAJO
          let newMeasure = {date: prevEnergy.date, value: cEnergy.value - prevEnergy.value + (Math.random() * (10 - 1) + 1) }; //ESTA SUMA random ES CODIGO DURO QUE HAY QUE ELIMINAR ES PARA TEST EN LO QU E JALA EL API   
          //let newMeasure = {date: prevEnergy.date, value: cEnergy.value - prevEnergy.value };
          eSavings.timeLine.push(newMeasure);
        }
      });
    }
    console.log("energyService", eSavings);
    return eSavings;
  }
  
  async getMeasurement(meas: Measurement, date: Date) {
    (await this.api.getMeasurement(meas)).subscribe(
      (data: any) => {
        if(data != undefined) {
          this.energy.total += data.sum; 
          let formatedDate = this.datepipe.transform(date, 'MMMM d') ?? "";
          this.energy.timeLine.push({date: formatedDate, value: data.sum})
        }
      },
      err => {
        console.log(err);
      });
    }
   
    getDatesBetween(minDate: string, maxDate: string): Date[] {
      let minDateL:Date = moment(minDate).toDate();
      let maxDateL:Date = moment(maxDate).toDate();
      console.log("Probe", minDateL.getDate());
      console.log("Probe", maxDateL.getDate());
      let dates: Date[] = [];
      //let daysBetween = maxDateL.getDate() - minDateL.getDate();
      let daysBetween = Math.floor((maxDateL.getTime() - minDateL.getTime()) / 1000 / 60 / 60 / 24);
      //if (daysBetween ==0){daysBetween = 1;} //para el caso de calculos en el mismo dia
      console.log("Probe", daysBetween);
      let initial = true;
      for(let x = 0; x <= daysBetween; x++) {
        let tminDate = minDateL;
        let newDate = initial == true ? tminDate.setDate(minDateL.getDate()) : tminDate.setDate(minDateL.getDate() + 1);
        dates.push(new Date(newDate));
        initial = false;
        console.log("Probe", "iteration")
      }
      console.log("Probe", dates);
      return dates;
    }

    getDateToCompare(originDate: Date, daysDiff: number): Date{
      //daysDiff can give previous times with negative numbers or future times with positives
      let tmpDate = originDate;
      tmpDate.setDate(tmpDate.getDate()+daysDiff);
      return tmpDate;
    }
  }
  