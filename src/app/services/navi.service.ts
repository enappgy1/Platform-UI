import { Injectable } from '@angular/core';

/*
THIS SERVICE WILL KEEP TRACK OF NAVIGATION UNDER DATA STRUCTURE 
*/
@Injectable()
export class NaviServices {
  currentLocation: string;
  objectId: string;
  buildingIndex: number;
  floorIndex: number;
  areaIndex: number;
  channelIndex: number;
  sensorIndex: number;
  naviFlag: number;

  constructor() {
    this.currentLocation = 'root'; //root, building, floor, area, channel
    this.objectId = 'NaN'; //id of the building, floor, area, channel, sensor in current review
    this.buildingIndex = 0;
    this.floorIndex = 0;
    this.areaIndex = 0;
    this.channelIndex = 0;
    this.sensorIndex = 0;
    this.naviFlag = 0; //let us know if data has been loaded
  }

  reset() {
    this.currentLocation = 'root'; //root, building, floor, area, channel
    this.objectId = 'NaN';
    this.buildingIndex = 0;
    this.floorIndex = 0;
    this.areaIndex = 0;
    this.channelIndex = 0;
    this.sensorIndex = 0;
    this.naviFlag = 0; //let us know if data has been loaded
  }

  getBreadcrumbs() {
    let breadcrumbs = [];
    let building = {};
    let floor = {};
    let area = {};
    let channel = {};
    switch (this.currentLocation) {
      case 'root':
        breadcrumbs = [];
        break;
      case 'building':
        building = {
          name: 'building',
          index: this.buildingIndex,
          value: 0,
        };
        breadcrumbs.push(building);
        break;
      case 'floor':
        building = {
          name: 'building',
          index: this.buildingIndex,
          value: 0,
        };
        breadcrumbs.push(building);
        floor = {
          name: 'floor',
          index: this.floorIndex,
          value: 0,
        };
        breadcrumbs.push(floor);
        break;
      case 'area':
        building = {
            name: 'building',
            index: this.buildingIndex,
            value: 0,
          };
          breadcrumbs.push(building);
          floor = {
            name: 'floor',
            index: this.floorIndex,
            value: 0,
          };
          breadcrumbs.push(floor);
          area = {
            name: 'area',
            index: this.areaIndex,
            value: 0,
          };
          breadcrumbs.push(area);
        break;
        case 'channel':
            building = {
                name: 'building',
                index: this.buildingIndex,
                value: 0,
              };
              breadcrumbs.push(building);
              floor = {
                name: 'floor',
                index: this.floorIndex,
                value: 0,
              };
              breadcrumbs.push(floor);
              area = {
                name: 'area',
                index: this.areaIndex,
                value: 0,
              };
              breadcrumbs.push(area);
              channel = {
                name: 'channel',
                index: this.channelIndex,
                value: 0,
              };
              breadcrumbs.push(channel);
            break;        
      default:
        //alert('You have reached the end of the data on the navigation service.');
        //console.log("You have reached the end of the data.");
        break;
    }
    return breadcrumbs;
  }

  getLocationChild() {
    let child: string = 'NaN';
    switch (this.currentLocation) {
      case 'root':
        child = 'building';
        break;
      case 'building':
        child = 'floor';
        break;
      case 'floor':
        child = 'area';
        break;
      case 'area':
        child = 'channel';
        break;
      default:
        child = 'none';
        //alert('You have reached the end of the data on the navigation service.');
        //console.log("You have reached the end of the data.");
        break;
    }
    return child;
  }

  setLocationChildIndex(index: number) {
    switch (this.currentLocation) {
      case 'root':
        this.buildingIndex = index;
        break;
      case 'building':
        this.floorIndex = index;
        break;
      case 'floor':
        this.areaIndex = index;
        break;
      case 'area':
        this.channelIndex = index;
        break;
      default:
        alert('You hae reached the end of the data.');
        break;
    }
  }

  isDataLoaded() {
    let value = false;
    if (this.naviFlag != null && this.naviFlag != 0) {
      value = true;
    }
    return value;
  }

  loadNavigationData() {
    this.loadCurrentLocation();
    this.loadObjectId();
    this.loadBuildingIndex();
    this.loadFloorIndex();
    this.loadAreaIndex();
    this.loadChannelIndex();
    this.loadSensorIndex();
    this.loadNaviFlag();
  }

  saveNavigationData() {
    this.saveCurrentLocation();
    this.saveObjectId();
    this.saveBuildingIndex();
    this.saveFloorIndex();
    this.saveAreaIndex();
    this.saveChannelIndex();
    this.saveSensorIndex();
    this.saveNaviFlag();
  }

  saveObjectId() {
    sessionStorage.setItem('objectId', this.objectId);
  }

  loadObjectId() {
    let objectId = sessionStorage.getItem('objectId');
    if (objectId != null) {
      this.objectId = objectId;
    }
  }

  saveNaviFlag() {
    sessionStorage.setItem('naviFlag', JSON.stringify(1));
  }

  loadNaviFlag() {
    let naviFlag = JSON.parse('' + sessionStorage.getItem('naviFlag'));
    if (naviFlag != null) {
      this.naviFlag = +naviFlag;
    }
  }

  saveCurrentLocation() {
    sessionStorage.setItem('currentLocation', this.currentLocation);
  }

  loadCurrentLocation() {
    var currentLocation = sessionStorage.getItem('currentLocation');
    if (currentLocation != null) {
      this.currentLocation = currentLocation;
    }
  }

  saveBuildingIndex() {
    sessionStorage.setItem('buildingIndex', JSON.stringify(this.buildingIndex));
  }

  loadBuildingIndex() {
    let index = JSON.parse('' + sessionStorage.getItem('buildingIndex'));
    if (index != null) {
      this.buildingIndex = +index;
    }
  }

  saveFloorIndex() {
    sessionStorage.setItem('floorIndex', JSON.stringify(this.floorIndex));
  }

  loadFloorIndex() {
    let index = JSON.parse('' + sessionStorage.getItem('floorIndex'));
    if (index != null) {
      this.floorIndex = +index;
    }
  }

  saveAreaIndex() {
    sessionStorage.setItem('areaIndex', JSON.stringify(this.areaIndex));
  }

  loadAreaIndex() {
    let index = JSON.parse('' + sessionStorage.getItem('areaIndex'));
    if (index != null) {
      this.areaIndex = +index;
    }
  }

  saveChannelIndex() {
    sessionStorage.setItem('channelIndex', JSON.stringify(this.channelIndex));
  }

  loadChannelIndex() {
    let index = JSON.parse('' + sessionStorage.getItem('channelIndex'));
    if (index != null) {
      this.channelIndex = +index;
    }
  }

  saveSensorIndex() {
    sessionStorage.setItem('sensorIndex', JSON.stringify(this.sensorIndex));
  }

  loadSensorIndex() {
    let index = JSON.parse('' + sessionStorage.getItem('sensorIndex'));
    if (index != null) {
      this.sensorIndex = +index;
    }
  }
}
