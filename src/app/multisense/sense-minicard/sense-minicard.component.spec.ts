import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SenseMinicardComponent } from './sense-minicard.component';

describe('SenseMinicardComponent', () => {
  let component: SenseMinicardComponent;
  let fixture: ComponentFixture<SenseMinicardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SenseMinicardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SenseMinicardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
