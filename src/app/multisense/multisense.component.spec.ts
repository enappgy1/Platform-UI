import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultisenseComponent } from './multisense.component';

describe('MultisenseComponent', () => {
  let component: MultisenseComponent;
  let fixture: ComponentFixture<MultisenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultisenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultisenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
