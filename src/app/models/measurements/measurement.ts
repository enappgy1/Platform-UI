import { calendarSearch } from "../calendarSearch.model";
import { BucketTimeWindow } from "./bucket-time-window.model";
import { MeasurementType } from "./measurementType.model";
import { ObjectType } from "./objectType.model";
import { SensorType } from "./sensorType.model";

export class Measurement {
    ObjectId: string; // Building, Floor, Area, Sensor
    ObjectType: ObjectType;
    SensorType: SensorType;
    CalendarSearch: calendarSearch;
    MeasurementType: MeasurementType;
    BucketTimeWindow: BucketTimeWindow;

    constructor(){
        this.ObjectId = "";
        this.SensorType = SensorType.Occupancy;
        this.CalendarSearch = new calendarSearch();
        this.MeasurementType = MeasurementType.Average;
        this.ObjectType = ObjectType.Building;
        this.BucketTimeWindow = BucketTimeWindow.Day;
    }
}