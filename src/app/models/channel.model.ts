import { Coordinates } from "./coordinates.model";
import { Databucket } from "./databucket.model";
import { DataPool } from "./dataPool.model";
import Dictionary from "./dictionary.model";

export class Channel{
    id:string;
    description:string = 'NaN';
    sensorType:string;
    //coordinates: Coordinates;
    databuckets: Databucket[];
    dataPools = new Dictionary<Databucket[]>();
    dataTotal: number = 0;
    archilogicId: string = 'NaN';
    floorPlanId: string = 'NaN';

    constructor(jsonObject:any){
        this.id = jsonObject.id;
        if(jsonObject.description == null || typeof jsonObject.description == 'undefined'){
            this.description = this.id;
        }
        this.sensorType = jsonObject.sensorType;
        // TODO: Data from metadatada don't have this values. 
        // this.coordinates = new Coordinates(
        //     jsonObject.coordinate.x,
        //     jsonObject.coordinate.y  
        // );

        this.databuckets = [];  
        console.log("channel", jsonObject)
        if(typeof jsonObject.dataBucket != 'undefined'){
          let databucket = new Databucket(jsonObject.dataBucket);
          this.databuckets.push(databucket);
        }

        if(typeof jsonObject.floorPlanId != 'undefined'){
          this.floorPlanId = jsonObject.floorPlanId;
          this.archilogicId = jsonObject.floorPlanId;
        }
    }

    builder(jsonObject: any){
        this.id = jsonObject.id;
        this.description = jsonObject.description; 
        this.sensorType = jsonObject.sensorType;   
        //this.coordinates = jsonObject.coordinates;
      }     

    
    setDataPool(sensorType:string, jsonObject:any){
        let databuckets: Databucket[];
        databuckets = [];
    
        if(jsonObject != null){      
          for(let i=0; i< jsonObject.length; i++){                                
              let databucket = new Databucket(jsonObject[i]);
              databuckets.push(databucket);
          }
          this.dataPools.add(sensorType, databuckets);
            return true;
        } else {
            return false;
        }
    }

    getDataPool(){
        return this.dataPools;
    }    

    sumDatabuckets(typeOfValue:string){
        let value: number;
        value = 0;
        for(let i=0; i<this.databuckets.length; i++){
            value += this.databuckets[i].getValue(typeOfValue);
        }
        return value;
    }

    getTrimDatabucketTotal(
        sensorType: string,
        typeOfValue: string,
        fromIndex: number,
        tillIndex: number
      ) {
        let total = 0;
        let databuckets: Databucket[];
        databuckets = this.dataPools.getItem(sensorType); //return databuckets
        if(tillIndex == 0){
            tillIndex = databuckets.length;
          }
        for (let i = fromIndex; i < tillIndex; i++) {
          total += databuckets[i].getValue(typeOfValue);
        }
        this.dataTotal= total;
        return total;
    }
}