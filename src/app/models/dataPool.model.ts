import { Databucket } from "./databucket.model";

export class DataPool{
    id:string; //sensoryType
    description:string; //sensorType description
    databuckets: Databucket[];

    constructor(id:string, description:string){
        this.id = id;
        this.description = description;
        this.databuckets = [];
    }

    setDatabuckets(jsonObject:any){
        this.databuckets = [];
        if(jsonObject != null){
            for(let i=0; i< jsonObject.length; i++){
                let databucket = new Databucket(jsonObject[i]);
                this.databuckets.push(databucket);
            }
            return true;
        } else {
            return false;
        }
    }

    getDatabuckets(){
        return this.databuckets;
    }

    sumDatabuckets(typeOfValue:string){
        let value: number;
        value = 0;
        for(let i=0; i<this.databuckets.length; i++){
            value += this.databuckets[i].getValue(typeOfValue);
        }
        return value;
    }

    getTrimArray(typeOfValue: string, fromIndex: number, tillIndex: number) {
        let total = 0;
        let myArray = [];
    
        if(tillIndex == 0){
          tillIndex = this.databuckets.length;
        }
        for(let i=fromIndex; i<tillIndex; i++){            
          myArray.push(this.databuckets[i].getValue(typeOfValue));
        }   
        
        return myArray;
      }
}