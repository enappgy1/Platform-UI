import { DatePipe } from '@angular/common';
import moment from 'moment';

export class calendarSearch {
  minDate: Date;
  maxDate: Date;
  timeInterval: [string, string];
  weekDays: string[] = new Array();
  private DatePipe: DatePipe = new DatePipe('en-EU');

  constructor() {
    this.minDate = new Date();
    //console.log("calendarSearch", this.minDate);
    //this.minDate.setDate(this.minDate.getDate()-1); //by default last 24 hs
    this.maxDate = new Date();
    this.timeInterval = ['00:00:00', '00:00:00']; //["00:00:01", "23:59:59"]; //["",""];
    //let currentDay = this.DatePipe.transform(this.maxDate, 'EEEE') ?? "";
    //this.weekDays = new Array(currentDay);
    this.weekDays = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday',
    ];
  }
  setValues(
    minDate: Date,
    maxDate: Date,
    timeInterval: [string, string],
    weekDays: []
  ) {
    this.minDate = minDate;
    this.maxDate = maxDate;
    this.timeInterval = timeInterval;
    this.weekDays = weekDays;
  }

  setMinDate(date: Date) {
    this.minDate = date;
  }

  setMaxDate(date: Date) {
    this.maxDate = date;
  }

  getMinDate(): Date {
    return this.minDate;
  }

  getMaxDate(): Date {
    return this.maxDate;
  }

  getNumberOfDaysConsideredInCalculus(): number {
    let numberOfDays: number = 0;
    //en teoria el API deberia indicar esto ya que aunque definas 5 dias si eliges solo lunes y solo hay 1, solo considerara 1 conviene que se indique en el JSON array
    //de lo contrario necesitaremos importar librerias y hacer un calculo aca
    //de momento lo dejaremos sencillo aunque no correcto
    let time = this.maxDate.getTime() - this.minDate.getTime();
    numberOfDays = time / (1000 * 3600 * 24); //difference in days
    if (numberOfDays <= 0) {
      numberOfDays = 1;
    } else {
      numberOfDays += 1;
    } //we add 1 to consider "today"
    
    return numberOfDays;
  }
}
