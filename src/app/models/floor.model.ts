import { Area } from './area.model';
import { Databucket } from './databucket.model';
import { DataPool } from './dataPool.model';
import Dictionary from './dictionary.model';

export class Floor {
  id: string;
  description: string = 'NaN';
  floorPlan: string;
  areas: Area[];
  databuckets: Databucket[];
  dataPools = new Dictionary<Databucket[]>();
  dataTotal: number = 0;
  archilogicId: string = 'NaN';
  floorPlanId: string = 'NaN';
  energyConsumption: string = ''; 
  energySavings: string = '';

  constructor(jsonObject: any) {
    this.id = jsonObject.id;
    if(typeof jsonObject.description != 'undefined'){
      this.description = jsonObject.description;
    }
    this.floorPlan = jsonObject.floorPlanId;

    this.databuckets = [];  
    console.log("Floor", jsonObject)
    if(typeof jsonObject.dataBucket != 'undefined'){
      let databucket = new Databucket(jsonObject.dataBucket);
      this.databuckets.push(databucket);
    }

    if(typeof jsonObject.floorPlanId != 'undefined'){
      this.floorPlanId = jsonObject.floorPlanId;
      this.archilogicId = jsonObject.floorPlanId;
    }

    this.areas = [];    
    if(typeof jsonObject.areas != 'undefined' && jsonObject.areas != null){
      this.setAreas(jsonObject.areas);
    }
  }

  builder(jsonObject: any) {
    this.id = jsonObject.id;
    this.description = jsonObject.description;
    this.floorPlan = jsonObject.floorPlan;
    this.setAreas(jsonObject.areas);
  }
  setAreas(jsonObject: any) {
    this.areas = [];
    if (jsonObject != null) {
      for (let i = 0; i < jsonObject.length; i++) {
        let area = new Area(jsonObject[i]);
        this.areas.push(area);
      }
      return true;
    } else {
      return false;
    }
  }

  setDataPool(sensorType:string, jsonObject:any){
    let databuckets: Databucket[];
    databuckets = [];
    if(jsonObject != null){   
      if(this.dataPools.containsKey(sensorType)){
        this.dataPools.removeItem(sensorType);
      }   
      for(let i=0; i< jsonObject.length; i++){                                
          let databucket = new Databucket(jsonObject[i]);
          databuckets.push(databucket);
      }
      this.dataPools.add(sensorType, databuckets);
        return true;
    } else {
        return false;
    }
  }

  getDataPool() {
    return this.dataPools;
  }
  
  XXXsumDatabuckets(typeOfValue: string) {
    let value: number;
    value = 0;
    for (let i = 0; i < this.databuckets.length; i++) {
      value += this.databuckets[i].getValue(typeOfValue);
    }
    return value;
  }

  getDatabucketTotal(typeOfValue: string) {
    let total = 0;
    for (let i = 0; i < this.databuckets.length; i++) {
      total += this.databuckets[i].getValue(typeOfValue);
    }
    return total;
  }

  getTrimDatabucketTotal(
    sensorType: string,
    typeOfValue: string,
    fromIndex: number,
    tillIndex: number
  ) {
    let total = 0;
    let databuckets: Databucket[];
    databuckets = this.dataPools.getItem(sensorType);
    if(tillIndex == 0){
        tillIndex = databuckets.length;
      }
    for (let i = fromIndex; i < tillIndex; i++) {
      total += databuckets[i].getValue(typeOfValue);
    }
    this.dataTotal = total;
    return total;
  }
}