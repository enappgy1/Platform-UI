import { DatePipe } from '@angular/common';

export class Energy {
  from!: Date;
  total: number;
  timeLine: { date: string; value: number }[]; // Days of week and respective value
  baseline: number;

  datepipe: DatePipe = new DatePipe('en-EU');

  constructor() {
    this.from = new Date();
    this.total = 0;
    this.timeLine = [];
    this.baseline = 1755.36;
  }

  getBaseline(): number {
    return this.baseline;
  }

  setTimeLine(jsonData: any) {
    if (jsonData != null) {
      for (let i = 0; i < jsonData; i++) {
        let stringDate: string = jsonData.from;
        let tmpValue: number = jsonData.sum;

        let tmpDate: Date = new Date(stringDate);
        let formatedDate = this.datepipe.transform(tmpDate, 'MMMM d') ?? '';

        let tmpTimeLine = { date: formatedDate, value: tmpValue};
        this.timeLine.push(tmpTimeLine);
      }
    }
  }

  getTimeLine(){
    return this.timeLine;
  }

  getTimeLineSorted(): any[] {
    let tmpTimeline: any;
    tmpTimeline = this.timeLine.sort((a, b) => a.date.localeCompare(b.date));

    return tmpTimeline;
  }

  consolidatedTimeLine(): any[] {
    let tmpTimeline: any[] = [];
    let tmpTimeLineConsolidated: any[] = [];

    let tmpDate: string = '';
    let tmpValue: number = 0;

    console.log('energy', this.timeLine);
    tmpTimeline = this.getTimeLineSorted();
    console.log('energy', tmpTimeline);

    let i: number = 0;
    tmpTimeline.forEach((res) => {
      if (i == 0) {
        tmpDate = res.date;
      }
      if (tmpDate === res.date) {
        console.log(
          'energy',
          'Same dates! Adding: ' + tmpValue + ' + ' + res.value
        );
        tmpValue += res.value;
        tmpDate = res.date;
      } else {
        let registry = { date: res.date, value: tmpValue };
        console.log('energy', registry);
        tmpTimeLineConsolidated.push(registry);
        tmpDate = res.date;
        tmpValue = res.value;
      }
      if (i == tmpTimeline.length - 1) {
        let registry = { date: res.date, value: tmpValue };
        console.log('energy', registry);
        tmpTimeLineConsolidated.push(registry);
      }
      i++;
    });
    console.log('energy', tmpTimeLineConsolidated);
    return tmpTimeLineConsolidated;
  }
}
