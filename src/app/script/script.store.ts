interface Scripts {
    name: string;
    src: string;
}  
export const ScriptStore: Scripts[] = [
    {name: 'archilogic2D', src: 'https://code.archilogic.com/fpe-sdk/v3.0.x/fpe.js'},
];