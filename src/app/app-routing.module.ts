import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataNavigationComponent } from './data-navigation/data-navigation.component';
import { ErrorComponent } from './error/error.component';
import { ProfileComponent } from 'src/app/pages/profile/profile.component';
import { AuthGuard } from '@auth0/auth0-angular';
import { ExternalApiComponent } from './pages/external-api/external-api.component';
import {HomeComponent} from './home/home.component';
import {UtilizationComponent} from './utilization/utilization.component';
import { LockedServiceComponent } from './locked-service/locked-service.component';
import {LoginComponent} from './login/login.component';
import { MultisenseComponent } from './multisense/multisense.component';
//import { ErrorComponent } from './error/error.component';
//import { LoginGuardian } from './login/login-guard.service';
//import { LoginComponent } from './login/login.component';
//import { FormularioComponent } from './personas/formulario/formulario.component';

const routes: Routes = [
    {path: '', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
    {path: 'external-api', component: ExternalApiComponent},
    {path: 'data-navigation', component: DataNavigationComponent, 
      children: [
    ]},  
    {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'utilization', component: UtilizationComponent, canActivate: [AuthGuard]},
    {path: 'multisense', component: MultisenseComponent, canActivate: [AuthGuard]},
    {path: 'impact', component: LockedServiceComponent, canActivate: [AuthGuard]},
    {path: 'locked-service/:service', component: LockedServiceComponent, canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent},
    {path: '**', component: ErrorComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
