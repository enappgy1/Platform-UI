import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavingsCategoriesComponent } from './savings-categories.component';

describe('SavingsCategoriesComponent', () => {
  let component: SavingsCategoriesComponent;
  let fixture: ComponentFixture<SavingsCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavingsCategoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavingsCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
