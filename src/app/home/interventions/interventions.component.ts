import { ExpansionPanelComponent } from "@progress/kendo-angular-layout";
import { Component, OnInit, QueryList, ViewChild, ViewChildren } from "@angular/core";
@Component({
  selector: 'app-interventions',
  templateUrl: './interventions.component.html',
  styleUrls: ['./interventions.component.css']
})
export class InterventionsComponent implements OnInit {

  public data: {icon: string, title:string, explanation: string, date: string, savings: number}[] = [
    {
      icon: 'k-i-brightness-contrast',
      title: 'Sun radiation on west wing',
      explanation: 'Sun radiation was detected on the west wing, so Enappgy reduced the heating. You saved 120 kwh',
      date: '12/02',
      savings: 122
    },
    {
      icon: 'k-i-wrench',
      title: 'High wind speeds',
      explanation: 'High wind speed were detected, so Enappgy disabled the window shade funution. You saved 120 EUR in reparation costs.',
      date: '01/02',
      savings: 58
    },
    {
      icon: 'k-i-user',
      title: 'Low occupancy on second floor', 
      explanation: 'There was low occupancy in area 4 on the second floor, so Enappgy lowered the heating and lightning in this area. You saved 230 kwh',
      date: '01/03',
      savings: 322
    }
  ]

  
  constructor() { }

  ngOnInit(): void {
  }

}
