import { Component, ContentChildren, ElementRef, Input, OnInit, ViewChild, ViewChildren} from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { DataServices } from 'src/app/services/data.service';
import { Building } from 'src/app/models/building.model';
import { ArchilogicServices } from 'src/app/services/archilogic.service';
import { NaviServices } from 'src/app/services/navi.service';
import { ActivatedRoute } from '@angular/router';
import { Area } from 'src/app/models/area.model';
import { Floor } from 'src/app/models/floor.model';
import { Databucket } from 'src/app/models/databucket.model';
import { faPlug, faUser, faCloud } from '@fortawesome/free-solid-svg-icons';
import { calendarSearch } from 'src/app/models/calendarSearch.model';
import { SensorType } from '../models/measurements/sensorType.model';
import { MeasurementType } from '../models/measurements/measurementType.model';
import { ObjectType } from '../models/measurements/objectType.model';
import  Dictionary   from "../models/dictionary.model";

declare const FloorPlanEngine: any;

@Component({
  selector: 'app-utilization',
  templateUrl: './utilization.component.html',
  styleUrls: ['./utilization.component.css']
})
export class UtilizationComponent implements OnInit {
  @ViewChild('map') map!: ElementRef<HTMLElement>;
  @ViewChildren('map') map2!: ElementRef<HTMLElement>;
  @ContentChildren('map') map3!: ElementRef<HTMLElement>;

  @Input() cardSize!:string;
  @Input() id!:string;

  faUser = faUser;
  faCloud = faCloud;
  faPlug = faPlug;
  publishableToken:string;
  sceneId:string;
  floorPlan:any;
  buildings: Building[] = [];
  navi: NaviServices;
  localAccountService: AccountService;
  minDate: Date = new Date();
  maxDate: Date = new Date();
  focusedDate: Date = new Date();
  from: Date = new Date();
  till: Date = new Date();
  floorIndex: number = 0;
  floor: Floor = new Floor('');
  floorId:string = '';
  floorPlanId:string = '';
  selectedWeekDays: string[] = new Array();
  timeInterval: [number, number] = [1, 8];
  loadCalendar: boolean = false;
  calendarSearch: calendarSearch = new calendarSearch();
  currentFloor!: Floor;
  floors!: Floor[];
  dataPools = new Dictionary<Databucket[]>();
  public selectedFloor:number = 0;

  constructor(private api: DataServices, private accountService: AccountService, private archilogic: ArchilogicServices, private route: ActivatedRoute) {
    this.publishableToken = this.archilogic.getToken();
    this.sceneId = this.archilogic.getBuildingScene();
    this.navi = this.accountService.navi;
    this.localAccountService = this.accountService;
  }

  async ngOnInit() {
    this.loadBuilding();
  }

  async loadBuilding() {
    this.api.getBuildings().subscribe(
      (buildings: any) => {
        if(buildings.length > 0) {
          this.buildings = buildings;
          this.loadFloors(this.buildings[0].id);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  async loadFloors(floorId: string) {
    this.api.getFloors(floorId).subscribe(
    async (floors: any) => {
      console.log("utilization", floors);
      if(floors !== null) {
        this.floors = floors;
        this.floors = this.floors.sort((a, b) => a.description.localeCompare(b.description));
        this.currentFloor = this.floors[0];
        this.loadCalendar = true;
        await this.loadFloorPlan(this.currentFloor,0);
      }
    },
    err => {
      console.log(err);
    }
    );
  }

  async loadFloorPlan(floor: Floor, floor_number:number) {
    this.selectedFloor = floor_number;
    this.currentFloor = floor;
    const startupSettings = {
      theme: {
        elements: {
          space: {
            program: {
              work: { fill: [214, 234, 248] }
            }
          }
        }
      }
    };
    const container = document.getElementById('map');
    const floorPlan = new FloorPlanEngine(container, startupSettings);
    const publishableToken = this.publishableToken;
    console.log("utilization", this.currentFloor);
    console.log("utilization", publishableToken);
    await floorPlan.loadScene(this.currentFloor.floorPlanId, { publishableToken });
    if(floorPlan !== undefined){
    this.floorPlan = floorPlan;
    await this.loadData();
  }
  }

  async loadData() {
    console.log("utilization", this.currentFloor);
    if(this.currentFloor.areas != undefined) {
      this.currentFloor.areas.forEach(async area => {
        console.log("utilization", area);
        await this.getAreaData(area, this.calendarSearch);
      });
    }
  }

  async getAreaData(area: Area, search: calendarSearch) {
    let sensorType = SensorType.Occupancy;
    (await this.api.getMeasurement_Legacy(area.id, SensorType.Occupancy, search, MeasurementType.Average, ObjectType.Area)).subscribe(
      (data:any) => {
        area.databuckets = [];
        let databuckets: Databucket[] = [];
          let databucket = new Databucket(data);
          area.databuckets.push(databucket);
          databuckets.push(databucket);          
        if(databuckets.length > 0){
          this.dataPools.add(sensorType.toString(), databuckets);
          let areaValue = databucket.avg;
          let areaColor = this.evaluateData(areaValue);
          this.colorizeAreaByDescription(area.spaceId, areaColor);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  async SearchEvent(search: calendarSearch) {
    console.log("utilization", "New calendar event trigered...");
    this.calendarSearch = search;
    await this.loadData();
  }

  evaluateData(value:number): string{
    let color:string = 'grey';
    if(value > 1){
      value = 100;
    }
    else{
      value = value * 100;
    }
    if(value < 19){
      color = 'green';
    } else if (value >= 20 && value < 39){
      color = 'yellow';
    } else if (value >= 40 && value <= 59){
      color = 'orange';
    }  else if (value >= 60 && value <= 79){
      color = 'orangered';
    }  else if (value >= 80 && value <= 100){
      color = 'red';
    }
    return color;
  }

  colorizeArea(areaId:string, color:string) {
    for(let i = 0; i < this.floorPlan.resources.spaces.length; i++) {
      if(areaId == this.floorPlan.resources.spaces[i].id) {
        let node = this.floorPlan.resources.spaces[i].node;
        switch(color) {
          case 'green':
            node.setHighlight({fill: [169, 228, 144]});
            break;
          case 'yellow':
            node.setHighlight({fill: [255, 237, 174]});
            break;
          case 'orange':
            node.setHighlight({fill: [255, 209,143]});
            break;
          case 'orangered':
            node.setHighlight({fill: [255, 180,141]});
            break;
          case 'red':
            node.setHighlight({fill: [255,155,142]});
            break;
          default:
            node.setHighlight({fill: [229, 231, 233]});
            break;
        }
      }
    }
  }

  colorizeAreaByDescription(spaceId:string, color:string) {
    if(this.floorPlan !== undefined){
      if(this.floorPlan.resources.spaces.length > 0) {
        var node:any = null;
        this.floorPlan.resources.spaces.forEach((element: any) => {
          if(element.node.id !== undefined) {
          if(element.node?.id == spaceId) {
            node = element.node.id == spaceId ? element.node : null;
            switch(color) {
              case 'green':
                node.setHighlight({fill: [169, 228, 144]});
                break;
              case 'yellow':
                node.setHighlight({fill: [255, 237, 174]});
                break;
              case 'orange':
                node.setHighlight({fill: [255, 209,143]});
                break;
              case 'orangered':
                node.setHighlight({fill: [255, 180,141]});
                break;
              case 'red':
                node.setHighlight({fill: [255, 155,142]});
                break;
              default:
                //node.setHighlight({fill: [229, 231, 233]});
                break;
            }
          }
        }
      });
    }
  }
  }

  // TODO: Refactor the following two methods in a service
//   sumDatabuckets(typeOfValue:string){
//     let value: number;
//     value = 0;
//     for(let i=0; i<this.databuckets.length; i++){
//         value += this.databuckets[i].getValue(typeOfValue);
//     }
//     return value;
// }

//TODO: Validate calculations and returned value
getTrimDatabucketTotal(sensorType: string, typeOfValue: string, fromIndex: number, tillIndex: number) {
    let total = 0;
    let databuckets: Databucket[];
    databuckets = this.dataPools.getItem(sensorType.toString());
    if(tillIndex == 0){
        tillIndex = databuckets.length;
      }
    for (let i = fromIndex; i < tillIndex; i++) {
      total += databuckets[i].getValue(typeOfValue);
    }
    total = total/(tillIndex-fromIndex);
    //this.dataTotal = total;
    return total;
}
}