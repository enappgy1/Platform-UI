import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AccountService } from '../services/account.service';
import { faBuilding, faKaaba, faArrowRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-data-navigation-menu',
  templateUrl: './data-navigation-menu.component.html',
  styleUrls: ['./data-navigation-menu.component.css'],
})
export class DataNavigationMenuComponent implements OnInit {
  @Input() parentAccountService!: AccountService;
  @Output() parentAccountServiceChange = new EventEmitter<AccountService>();

  @Input() breadcrumb!: any;

  object: string = '';
  objectIndex: string = '';
  objectValue: number = 0;
  objectUnit: string = 'NaN';

  icon: any;
  faBuilding = faBuilding;
  faKaaba = faKaaba;
  faArrowRight = faArrowRight;

  constructor(private accountServices: AccountService) {
    //this.accountServices.loadFromSessionStorage();
    //this.objectUnit = this.accountServices.getUnitFromSensorType(this.accountServices.sensorType);
  }

  ngOnInit(): void {
    //this.objectUnit = this.parentAccountService.getUnitFromSensorType(this.parentAccountService.sensorType);
    //this.object = this.parentAccountService.
    console.log(this.breadcrumb);
    this.object = this.breadcrumb.name;
    this.objectIndex = this.breadcrumb.index;
    this.objectValue = this.breadcrumb.value;
    this.objectUnit = this.parentAccountService.getUnitFromSensorType(
      this.parentAccountService.sensorType
    );

    switch (this.object) {
      case 'building':
        this.icon = this.faBuilding;
        break;
      case 'floor':
        this.icon = this.faKaaba;
        break;
      case 'area':
        this.icon = this.faBuilding;
        break;
      case 'channel':
        this.icon = this.faBuilding;
        break;
    }
  }

  moveTo() {
    if (this.parentAccountService.navi.currentLocation != this.object) {
      this.parentAccountService.navi.currentLocation = this.object;
      this.parentAccountService.saveInSessionStorage();
    }
  }
}
