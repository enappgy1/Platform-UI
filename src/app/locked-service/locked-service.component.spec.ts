import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LockedServiceComponent } from './locked-service.component';

describe('LockedServiceComponent', () => {
  let component: LockedServiceComponent;
  let fixture: ComponentFixture<LockedServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LockedServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LockedServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
