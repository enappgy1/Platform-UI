import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
@Component({
  selector: 'app-locked-service',
  templateUrl: './locked-service.component.html',
  styleUrls: ['./locked-service.component.css']
})
export class LockedServiceComponent implements OnInit {

  public service: string | null = '';
  constructor(private activeRoute: ActivatedRoute) {}
  ngOnInit(): void {
    this.activeRoute.queryParams.subscribe(queryParams => {
      // do something with the query params
    });
    this.activeRoute.params.subscribe(routeParams => {
      this.service = this.activeRoute.snapshot.paramMap.get('service')
    });
    
  }

}
