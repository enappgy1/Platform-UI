import { Component, ViewEncapsulation } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Router, NavigationEnd } from "@angular/router";
import { FormGroup, FormControl } from "@angular/forms";
import { TextBoxComponent } from "@progress/kendo-angular-inputs";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string = 'Enappgy dashboard';
  public expanded = false; //by default we want it collapsed.
  public routes: any[] = [
    {
      path: '', 
      text: 'Home', 
      icon: 'k-i-thumbnails-up'
    },
    {
      path: 'utilization', 
      text: 'Utilization', 
      icon: 'k-i-accessibility'
    },
    {
      path: 'multisense', 
      text: 'Multisense', 
      icon: 'k-i-graph'
    },
    {
      path: 'impact',
      text: 'Impact',
      icon: 'k-i-calculator',
      icon_unlocked: 'k-i-calculator'
    },
    {
      path: 'locked-service/Environment', 
      text: 'Environment', 
      icon: 'k-i-lock',
      icon_unlocked: 'k-i-globe-outline'
    },
    {
      path: 'locked-service/Lighting',
      text: 'Lighting',
      icon: 'k-i-lock',
      icon_unlocked: 'k-i-brightness-contrast'
    },
    {
      path: 'locked-service/Booking System',
      text: 'Booking System',
      icon: 'k-i-lock',
      icon_unlocked: 'k-i-calendar'
    },
    {
      path: 'locked-service/Predictions',
      text: 'Predictions',
      icon: 'k-i-lock',
      icon_unlocked: 'k-i-heart-ouline'
    },
    {
      path: 'locked-service/Solar',
      text: 'Solar',
      icon: 'k-i-lock',
      icon_unlocked: 'k-i-heart-ouline'
    },
    {
      path: 'locked-service/Work Request',
      text: 'Work Request',
      icon: 'k-i-lock',
      icon_unlocked: 'k-i-heart-ouline'
    }

  ];

  public url: string = ''
  public emptyLayoutPages = ['/login']
  public showNavigation = true;
  public chooseLayout = () => {
    if(this.emptyLayoutPages.indexOf(this.url) == -1) {
      this.showNavigation = true;
    }
    else {
      this.showNavigation = false;
    }
    
  }
  

  constructor(public auth: AuthService, private router: Router) {
    
    this.routes[0].selected = true;
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          this.url = this.router.url;
          this.chooseLayout();
        }
      }
    );
  }
 

}
