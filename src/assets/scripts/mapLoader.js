export function addLoader(element) {
    const sceneEl = document.createElement('div')
    sceneEl.classList.add('scene')
    sceneEl.innerHTML = `<div class="cube">
    <div class="cube__face cube__face--front"></div>
    <div class="cube__face cube__face--back"></div>
    <div class="cube__face cube__face--right"></div>
    <div class="cube__face cube__face--left"></div>
  </div>`
    element.appendChild(sceneEl)
  }
  