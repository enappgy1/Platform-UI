import authConfig from '../../auth_config.prod.json';
export const environment = {
  production: true,
  auth: {
    domain: authConfig.domain,
    clientId: authConfig.clientId,
    redirectUri: window.location.origin,
    audience: authConfig.audience
  },
  enappgyData: {
    serverUrl: authConfig.serverUrl,
    tenantId: authConfig.tenantId,
    protectedMessage: authConfig.protectedMessage,
    metadataUrl : authConfig.metadataUrl,
    dataUrl : authConfig.dataUrl,
    publishableToken : authConfig.publishableToken
  }
};
